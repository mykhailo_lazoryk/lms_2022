from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from django import forms
from django.urls import reverse
from django.utils.html import format_html

from students.models import Student, Group, Teacher, UserProfile
from students.utils.admin_filter import AgeRangeListFilter


class ProfileAdminInline(admin.StackedInline):
    model = UserProfile


@admin.register(get_user_model())
class CustomerAdmin(UserAdmin):
    inlines = [ProfileAdminInline]
    fields = ("phone_number", "first_name", "last_name", "password", "email")
    list_display = ("phone_number", "first_name", "last_name", "email")
    fieldsets = None
    ordering = ("email",)
    readonly_fields = ("email",)


@admin.register(Student)
class StudentAdmin(admin.ModelAdmin):
    date_hierarchy = "birth_date"
    ordering = (
        "first_name",
        "group__name",
    )
    list_display = ("first_name", "last_name", "email", "age", "birth_date")
    list_display_links = ("email",)
    list_filter = (
        "birth_date",
        AgeRangeListFilter,
    )
    search_fields = ("first_name__istartswith",)


@admin.register(Teacher)
class TeacherAdmin(admin.ModelAdmin):
    list_display = ("first_name", "last_name", "email", "birth_date", "group_count", "links_to_groups")
    # fields = ('first_name', 'last_name', 'email', 'birth_date')
    fieldsets = (
        ("Personal info", {"fields": ("first_name", "last_name", "email")}),
        ("Additional info", {"classes": ("collapse",), "fields": ("birth_date", "group")}),
    )

    def group_count(self, obj):
        if obj.group:
            return obj.group.count()
        return 0

    def links_to_groups(self, obj):
        if obj.group:
            groups = obj.group.all()
            links = []
            for group in groups:
                links.append(
                    f"<a class='button' href='{reverse('admin:students_group_change', args=[group.pk])}'"
                    f">{group.name}</a>"
                )
            return format_html("</br></br>".join(links))
        return "No groups found"


class StudentAdminInline(admin.StackedInline):
    model = Student
    extra = 0


class TeacherAdminInline(admin.StackedInline):
    model = Teacher.group.through
    extra = 0


class GroupForm(forms.ModelForm):
    class Meta:
        model = Group
        fields = "__all__"

    comments = forms.CharField(widget=forms.Textarea)
    email = forms.EmailField()
    send_email = forms.BooleanField(required=False)

    def clean_max_count_of_students(self):
        if self.cleaned_data["max_count_of_students"] > 100:
            raise forms.ValidationError("Should be less than 100!!!")
        return self.cleaned_data["max_count_of_students"]

    def save(self, commit=True):
        print(self.cleaned_data["comments"])
        print(self.cleaned_data["email"])
        if self.cleaned_data["send_email"] and self.cleaned_data["email"]:
            print("I will send email")
            # TODO send email
        return super().save(commit=commit)


@admin.action(description="Set to zero of count of selected groups.")
def count_to_zero(model_admin, request, queryset):
    queryset.update(max_count_of_students=0)


@admin.register(Group)
class GroupAdmin(admin.ModelAdmin):
    inlines = [StudentAdminInline, TeacherAdminInline]
    form = GroupForm
    actions = [count_to_zero]
