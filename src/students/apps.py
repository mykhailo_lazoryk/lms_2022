from django.apps import AppConfig
from django.contrib.auth import get_user_model
from django.db.models.signals import post_save, pre_save


class StudentsConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "students"

    def ready(self):
        from students.services.signals import create_user_profile_signal, pre_save_student_signal
        from students.models import Student

        post_save.connect(create_user_profile_signal, sender=get_user_model())
        pre_save.connect(pre_save_student_signal, sender=Student)
