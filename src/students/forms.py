from django.contrib.auth import get_user_model
from django.contrib.auth.forms import UserCreationForm
from django.core.exceptions import ValidationError
from django.forms import ModelForm
from location_field.forms.plain import PlainLocationField

from students.models import Student


class StudentForm(ModelForm):
    class Meta:
        model = Student
        fields = ("first_name", "last_name", "email", "group")

    @staticmethod
    def normalize_text(text: str) -> str:
        return text.strip().capitalize()

    def clean_email(self):
        email = self.cleaned_data["email"]

        if "@yandex" in email.lower():
            raise ValidationError("Yandex domain is forbidden is our country!!!")

        return email

    def clean_last_name(self):
        return self.normalize_text(self.cleaned_data["last_name"])

    def clean_first_name(self):
        return self.normalize_text(self.cleaned_data["first_name"])

    def clean(self):
        cleaned_data = super().clean()

        first_name = cleaned_data["first_name"]
        last_name = cleaned_data["last_name"]

        if last_name == first_name:
            raise ValidationError("First name and last name cant be equal")

        return cleaned_data


class UserRegistrationForm(UserCreationForm):

    class Meta:
        model = get_user_model()
        fields = ("email", "phone_number", "password1", "password2")

    def clean(self):
        _cleaned_data = super().clean()

        if not bool(_cleaned_data["email"]) and not not bool(_cleaned_data["phone_number"]):
            raise ValidationError("Insert email or phone number. At least one of them, please")

        elif not bool(_cleaned_data["email"]):
            if get_user_model().objects.filter(phone_number=_cleaned_data["phone_number"]).exists():
                raise ValidationError("User with phone number already exists!!!")

        elif not bool(_cleaned_data["phone_number"]):
            if get_user_model().objects.filter(phone_number=_cleaned_data["email"]).exists():
                raise ValidationError("User with email already exists!!!")

        return _cleaned_data


class UserLocationForm(ModelForm):
    location = PlainLocationField(based_fields=["city"], initial="-22.2876834,-49.1607606", disabled=True)

    class Meta:
        model = get_user_model()
        fields = ("location",)
