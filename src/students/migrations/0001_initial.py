# Generated by Django 3.2.16 on 2023-01-25 18:35

from django.conf import settings
import django.core.validators
from django.db import migrations, models
import django.db.models.deletion
import django.db.models.manager
import django.utils.timezone
import students.managers
import students.utils.validators
import uuid


class Migration(migrations.Migration):
    initial = True

    dependencies = [
        ("auth", "0012_alter_user_first_name_max_length"),
    ]

    operations = [
        migrations.CreateModel(
            name="Customer",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("password", models.CharField(max_length=128, verbose_name="password")),
                ("last_login", models.DateTimeField(blank=True, null=True, verbose_name="last login")),
                (
                    "is_superuser",
                    models.BooleanField(
                        default=False,
                        help_text="Designates that this user has all permissions without explicitly assigning them.",
                        verbose_name="superuser status",
                    ),
                ),
                ("first_name", models.CharField(blank=True, max_length=150, verbose_name="name")),
                ("last_name", models.CharField(blank=True, max_length=150, verbose_name="surname")),
                ("email", models.EmailField(max_length=254, unique=True, verbose_name="email address")),
                (
                    "is_staff",
                    models.BooleanField(
                        default=False,
                        help_text="Designates whether the user can log into this admin site.",
                        verbose_name="staff status",
                    ),
                ),
                (
                    "is_active",
                    models.BooleanField(
                        default=True,
                        help_text="Designates whether this user should be treated as active. Unselect this instead of deleting accounts.",
                        verbose_name="active",
                    ),
                ),
                ("date_joined", models.DateTimeField(default=django.utils.timezone.now, verbose_name="date joined")),
                ("birth_date", models.DateField(null=True)),
                ("avatar", models.ImageField(upload_to="media/img/profiles")),
                (
                    "groups",
                    models.ManyToManyField(
                        blank=True,
                        help_text="The groups this user belongs to. A user will get all permissions granted to each of their groups.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.Group",
                        verbose_name="groups",
                    ),
                ),
                (
                    "user_permissions",
                    models.ManyToManyField(
                        blank=True,
                        help_text="Specific permissions for this user.",
                        related_name="user_set",
                        related_query_name="user",
                        to="auth.Permission",
                        verbose_name="user permissions",
                    ),
                ),
            ],
            options={
                "verbose_name": "user",
                "verbose_name_plural": "users",
            },
            managers=[
                ("objects", students.managers.CustomerManager()),
            ],
        ),
        migrations.CreateModel(
            name="Group",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("name", models.CharField(max_length=120, null=True)),
                ("max_count_of_students", models.PositiveIntegerField(default=20, null=True)),
            ],
        ),
        migrations.CreateModel(
            name="UserProfile",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                ("photo", models.ImageField(upload_to="static/img/profiles/")),
                ("phone_number", models.CharField(max_length=16)),
                ("birth_date", models.DateField(blank=True, null=True)),
                (
                    "user",
                    models.OneToOneField(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL),
                ),
            ],
        ),
        migrations.CreateModel(
            name="Teacher",
            fields=[
                ("id", models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name="ID")),
                (
                    "first_name",
                    models.CharField(
                        max_length=120,
                        null=True,
                        validators=[
                            django.core.validators.MinLengthValidator(2),
                            students.utils.validators.first_name_validator,
                        ],
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        max_length=120, null=True, validators=[django.core.validators.MinLengthValidator(2)]
                    ),
                ),
                ("email", models.EmailField(max_length=120, null=True)),
                ("birth_date", models.DateField(null=True)),
                ("department", models.CharField(max_length=300, null=True)),
                ("grade", models.CharField(max_length=150, null=True)),
                ("group", models.ManyToManyField(to="students.Group")),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="Student",
            fields=[
                (
                    "first_name",
                    models.CharField(
                        max_length=120,
                        null=True,
                        validators=[
                            django.core.validators.MinLengthValidator(2),
                            students.utils.validators.first_name_validator,
                        ],
                    ),
                ),
                (
                    "last_name",
                    models.CharField(
                        max_length=120, null=True, validators=[django.core.validators.MinLengthValidator(2)]
                    ),
                ),
                ("email", models.EmailField(max_length=120, null=True)),
                ("grade", models.PositiveIntegerField(default=0, null=True)),
                ("birth_date", models.DateField(null=True)),
                ("department", models.CharField(max_length=300, null=True)),
                (
                    "uuid",
                    models.UUIDField(
                        db_index=True,
                        default=uuid.uuid4,
                        editable=False,
                        primary_key=True,
                        serialize=False,
                        unique=True,
                    ),
                ),
                (
                    "group",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="all_students_in_this_group",
                        to="students.group",
                    ),
                ),
            ],
            options={
                "abstract": False,
            },
        ),
        migrations.CreateModel(
            name="ProxyUser",
            fields=[],
            options={
                "verbose_name": "Proxy User",
                "verbose_name_plural": "Proxy Users",
                "ordering": ("-pk",),
                "proxy": True,
                "indexes": [],
                "constraints": [],
            },
            bases=("students.customer",),
            managers=[
                ("people", django.db.models.manager.Manager()),
                ("objects", students.managers.CustomerManager()),
            ],
        ),
    ]
