# Generated by Django 3.2.16 on 2023-02-08 18:35

from django.db import migrations, models
import phonenumber_field.modelfields


class Migration(migrations.Migration):
    dependencies = [
        ("students", "0003_alter_student_options"),
    ]

    operations = [
        migrations.AddField(
            model_name="customer",
            name="phone_number",
            field=phonenumber_field.modelfields.PhoneNumberField(
                blank=True, max_length=128, null=True, region=None, verbose_name="phone number"
            ),
        ),
        migrations.AlterField(
            model_name="customer",
            name="email",
            field=models.EmailField(blank=True, max_length=254, null=True, verbose_name="email address"),
        ),
    ]
