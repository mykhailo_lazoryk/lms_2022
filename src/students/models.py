import datetime
from uuid import uuid4

from django.utils import timezone
from django.utils.translation import gettext_lazy as _

from django.contrib.auth import get_user_model
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.validators import UnicodeUsernameValidator
from django.core.validators import MinLengthValidator
from django.db import models
from faker import Faker
from location_field.models.plain import PlainLocationField
from phonenumber_field.modelfields import PhoneNumberField

from students.managers import PeopleManager, CustomerManager
from students.utils.validators import first_name_validator

from django.contrib.auth.models import PermissionsMixin


# 1. Proxy model
# 2. User profile
# 3. AbstractUser
# 4. AbstractBaseUser


class Customer(AbstractBaseUser, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()

    first_name = models.CharField(_("name"), max_length=150, blank=True)
    last_name = models.CharField(_("surname"), max_length=150, blank=True)
    email = models.EmailField(_("email address"), null=True, blank=True)
    phone_number = PhoneNumberField(_("phone number"), null=True, blank=True)
    is_staff = models.BooleanField(
        _("staff status"),
        default=False,
        help_text=_("Designates whether the user can log into this admin site."),
    )
    is_active = models.BooleanField(
        _("active"),
        default=True,
        help_text=_(
            "Designates whether this user should be treated as active. " "Unselect this instead of deleting accounts."
        ),
    )
    date_joined = models.DateTimeField(_("date joined"), default=timezone.now)
    birth_date = models.DateField(null=True)
    avatar = models.ImageField(upload_to="media/img/profiles")
    city = models.CharField(max_length=255, blank=True)
    location = PlainLocationField(based_fields=["city"], zoom=7, blank=True)

    objects = CustomerManager()

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def clean(self):
        super().clean()
        self.email = self.__class__.objects.normalize_email(self.email)

    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = "%s %s" % (self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        return self.first_name

    def get_working_time(self):
        return f"Time on site: {timezone.now() - self.date_joined}"


class ProxyUser(get_user_model()):
    people = PeopleManager()

    class Meta:
        proxy = True
        ordering = ("-pk",)
        verbose_name = "Proxy User"
        verbose_name_plural = "Proxy Users"

    def do_something(self):
        print(f"{self.first_name} + something")


class UserProfile(models.Model):
    user = models.OneToOneField(get_user_model(), on_delete=models.CASCADE)
    photo = models.ImageField(upload_to="static/img/profiles/")
    phone_number = models.CharField(max_length=16)
    birth_date = models.DateField(blank=True, null=True)


class Person(models.Model):
    first_name = models.CharField(max_length=120, null=True, validators=[MinLengthValidator(2), first_name_validator])
    last_name = models.CharField(max_length=120, null=True, validators=[MinLengthValidator(2)])
    email = models.EmailField(max_length=120, null=True)
    grade = models.PositiveIntegerField(default=0, null=True)
    birth_date = models.DateField(null=True)
    department = models.CharField(max_length=300, null=True)

    class Meta:
        abstract = True


class Student(Person):
    uuid = models.UUIDField(primary_key=True, editable=False, default=uuid4, unique=True, db_index=True)
    group = models.ForeignKey(to="students.Group", related_name="all_students_in_this_group", on_delete=models.CASCADE)

    class Meta:
        verbose_name = "My student"
        verbose_name_plural = "All students"
        ordering = ("first_name", "last_name")

    @property
    def age(self):
        return datetime.datetime.now().year - self.birth_date.year

    def __str__(self):
        return f"{self.first_name}_{self.last_name} ({self.pk})"

    @classmethod
    def generate_instances(cls, count):
        faker = Faker()
        for i in range(count):
            cls.objects.create(
                first_name=faker.first_name(),
                last_name=faker.last_name(),
                email=faker.email(),
                group=Group.objects.first(),
                birth_date=faker.date_time_between(start_date="-30y", end_date="-18y"),
            )


class Group(models.Model):
    name = models.CharField(max_length=120, null=True)
    max_count_of_students = models.PositiveIntegerField(default=20, null=True)

    def __str__(self):
        return f"{self.name}"


class Teacher(Person):
    grade = models.CharField(max_length=150, null=True)
    group = models.ManyToManyField(to="students.Group")

    def __str__(self):
        return f"{self.first_name}_{self.last_name}"
