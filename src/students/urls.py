from django.urls import path

from students.views import (
    first_view,
    get_students,
    test,
    CreateStudentView,
    UpdateStudentView,
    UserLogin,
    UserLogout,
    UserRegistration,
    ActivateUser,
    search_history,
)

app_name = "students"

urlpatterns = [
    path("details/", first_view, name="index"),
    path("", get_students, name="get_students"),
    path("create/", CreateStudentView.as_view(), name="create_student"),
    path("update/<uuid:uuid>/", UpdateStudentView.as_view(), name="update_student"),
    path("login/", UserLogin.as_view(), name="login"),
    path("logout/", UserLogout.as_view(), name="logout"),
    path("registration/", UserRegistration.as_view(), name="registration"),
    path("activate/<str:uuid64>/<str:token>/", ActivateUser.as_view(), name="activate_user"),
    # path("send-test-email/", send_test_email, name="send_test_email"),
    path("test/", test, name="test_view"),
    path("history/", search_history, name="history"),
]

# localhost:8000/students/ - all
# localhost:8000/students/12/ - one element
# localhost:8000/students/create/ - create element
# localhost:8000/students/update/12/ - update element
# localhost:8000/students/delete/12/ - delete element
