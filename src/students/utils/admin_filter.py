from datetime import date

from django.contrib import admin


class AgeRangeListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = "Age range filter"

    # Parameter for the filter that will be used in the URL query.
    parameter_name = "age_range"

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return [(1, "5-21"), (2, "22-35"), (3, "35-60")]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        # Compare the requested value (either '80s' or '90s')
        # to decide how to filter the queryset.
        if self.value() == "1":
            return queryset.filter(
                birth_date__gte=date(1970, 1, 1),
                birth_date__lte=date(1980, 1, 1),
            )
        if self.value() == "2":
            return queryset.filter(
                birth_date__gte=date(1980, 1, 1),
                birth_date__lte=date(1990, 1, 1),
            )
