import datetime
import logging

LOGGER = logging.getLogger("custom_logger")


class CustomMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        print("Before my view")
        LOGGER.debug(f"Before request from {request.user}")
        setattr(request, "current_time", datetime.datetime.now())
        response = self.get_response(request)
        print("After my view")

        return response
