import datetime

from django.contrib.auth import login, get_user_model, authenticate
from django.contrib import messages
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.db.models import Q
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.encoding import force_str
from django.utils.http import urlsafe_base64_decode
from django.views.generic import TemplateView, CreateView, UpdateView, RedirectView

from students.forms import StudentForm, UserRegistrationForm
from students.models import Student

from webargs import fields
from webargs.djangoparser import use_kwargs

from students.services.emails import send_registration_email
from students.utils.token_generator import TokenGenerator


# FBV
# CBV


class IndexView(TemplateView):
    template_name = "index.html"
    http_method_names = ["get"]

    # extra_context = {"form": UserRegistrationForm}

    def get(self, request, *args, **kwargs):
        print(f"In my view: {request.current_time}")

        if request.user.is_authenticated:
            messages.success(request, "User is available")
        else:
            messages.warning(request, "Please login")

        return super().get(self, request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IndexView, self).get_context_data(**kwargs)
        # context["form"] = UserLocationForm(instance=self.request.user)
        return context


def first_view(request):
    print(request)
    return HttpResponse("Hello from view")


@use_kwargs(
    {
        "first_name": fields.Str(required=False),
        "last_name": fields.Str(required=False),
        "search_text": fields.Str(required=False),
    },
    location="query",
)
def get_students(request, **kwargs):
    students = Student.objects.all()
    search_fields = ["last_name", "first_name", "email"]

    for field_name, field_value in kwargs.items():
        if field_name == "search_text":
            request.session[f"search_text_{datetime.datetime.now()}"] = field_value
            or_filter = Q()
            for field in search_fields:
                # accumulate filter condition
                or_filter |= Q(**{f"{field}__icontains": field_value})
            students = students.filter(or_filter)
        else:
            if field_value:
                students = students.filter(**{field_name: field_value})

    return render(request, template_name="student/students_list.html", context={"students": students})


def search_history(request):
    log_list = {}
    for key, value in request.session.items():
        if "search_text_" in key:
            log_list.update({key: value})

    return render(request, template_name="search_history.html", context={"search_logs": log_list})


class CreateStudentView(LoginRequiredMixin, CreateView):
    template_name = "student/students_create.html"
    # model = Student
    form_class = StudentForm
    # fields = ["first_name", "last_name", "email", "group"]
    success_url = reverse_lazy("index")
    initial = {"first_name": "Student", "last_name": "Student"}


# def create_student(request):
#     if request.method == "POST":
#         form = StudentForm(request.POST)
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse("students:get_students"))
#     else:
#         form = StudentForm()
#     return render(request, template_name="student/students_create.html", context={"form": form})


class UpdateStudentView(LoginRequiredMixin, UpdateView):
    template_name = "student/students_edit.html"
    form_class = StudentForm
    success_url = reverse_lazy("index")
    pk_url_kwarg = "uuid"
    queryset = Student.objects.all()

    def get_queryset(self):
        self.request.GET


# def update_student(request, pk):
#     student = get_object_or_404(Student.objects.all(), pk=pk)
#
#     if request.method == "POST":
#         form = StudentForm(request.POST, instance=student)
#         if form.is_valid():
#             form.save()
#             return HttpResponseRedirect(reverse("students:get_students"))
#     else:
#         form = StudentForm(instance=student)
#     return render(request, template_name="student/students_edit.html", context={"form": form})


def test(request):
    user = get_user_model()()
    user.username = "test2"
    user.set_password("password")
    user.save()
    login(request, user)

    return HttpResponse(user)


class UserLogin(LoginView):
    # next_page = '/'
    ...


class UserLogout(LogoutView):
    ...


class UserRegistration(CreateView):
    template_name = "registration/create_user.html"
    form_class = UserRegistrationForm
    success_url = reverse_lazy("index")

    def form_valid(self, form):
        self.object = form.save(commit=False)
        self.object.first_name = "Mykhailo"
        user = form.save()
        login(self.request, user=user, backend="students.auth_backend.AuthBackend")
        # authenticate.
        # send_registration_email(request=self.request, user_instance=self.object)

        return redirect(self.success_url)


# 1. uniq. email, username, phone_number, номер паспорта, ІПН
# 2. password, sms, token, faceid, fingerprint, pin, graphic, file, flash drive.

# 1. not authorized
# login
# registration
# reset password
# save login
# registration via email

# 2. authorized
# user profile
# settings
# > change password
# ...
# logout
# show username + photo

# 3. admin
# user profile
# link to admin panel
# settings
# > change password
# ...
# logout
# show username + photo


class ActivateUser(RedirectView):
    url = reverse_lazy("index")

    def get(self, request, uuid64, token, *args, **kwargs):
        try:
            pk = force_str(urlsafe_base64_decode(uuid64))
            current_user = get_user_model().objects.get(pk=pk)
        except (ValueError, get_user_model().DoesNotExist, TypeError):
            return HttpResponse("Wrong data!!!")

        if current_user and TokenGenerator().check_token(current_user, token):
            current_user.is_active = True
            current_user.save()

            login(request, current_user)

            return super().get(request, *args, **kwargs)

        return HttpResponse("Wrong data!!!")

# def send_test_email(request):
#     send_mail(
#         subject="Hello from LMS",
#         message="Hello from LMS message",
#         from_email=settings.EMAIL_HOST_USER,
#         recipient_list=[
#             settings.EMAIL_HOST_USER,
#             "igor.samarsky8@gmail.com",
#             "serhiyturchyn@gmail.com",
#             "antonriabkov11@gmail.com",
#             "2013.olga.yudina@gmail.com",
#         ],
#     )
#     return HttpResponse("Done")
